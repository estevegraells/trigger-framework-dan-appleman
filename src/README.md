# Cuál es el propósito de este repositorio? # 

Dentro de de una serie de artículos sobre Frameworks y patrones de gestión de triggers, este es el repositorio con el código correspondiente a la entrada del blog [El  framework de Appleman que los inició a todos](http://forcegraells.com/2018/06/19/framework-triggers-appleman/). 

Incluye las clases creadas por Dan Appleman, con una pequeña adaptación y leves modificaciones para clarificar el código, y una clase de test muy simple para realizar el test.

Espero que te sea de ayuda.

# Contacto #

Para cualquier duda, sugerencia o comentario puedes contactarme en mi dirección de [Email Personal](esteve.graells@gmail.com).

Un saludo.
