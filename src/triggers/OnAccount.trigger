/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description: Trigger para Account que invoca al Dispatcher
 */

trigger OnAccount on Account (
        before insert, before update,
        before delete, after insert,
        after update, after delete, after undelete) {

    // Llamada al dispatcher del patron de Tony Scott
    // TriggerFactory.createAndExecuteHandler(AccountHandler2.class);

    // Llamada al dispatcher del framework de Dan Appleman
    GlobalDispatcher.run('Account',
            trigger.isBefore, trigger.isAfter,
            trigger.isInsert, trigger.isUpdate,
            trigger.isDelete, trigger.isExecuting,
            trigger.new, trigger.newMap,
            trigger.old, trigger.oldMap);
}