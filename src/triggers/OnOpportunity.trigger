/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description:
 *
 */

trigger OnOpportunity on Opportunity (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {


    //Invocar al dispatcher centralizador
    GlobalDispatcher.run(
            'Opportunity',
            trigger.isBefore,
            trigger.isAfter,
            trigger.isInsert,
            trigger.isUpdate,
            trigger.isDelete,
            trigger.isExecuting,
            trigger.new, trigger.newMap,
            trigger.old, trigger.oldMap);
}