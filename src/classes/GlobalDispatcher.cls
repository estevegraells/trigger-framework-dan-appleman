/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description: Dispatcher que centraliza todas las llamadas de triggers,
 * gestiona las re-entradas y realiza enrutamiento hacia el Handler
 *
 */

public with sharing class GlobalDispatcher {

    public static Map<ID, Account> acsToUpdate = new Map<ID, Account>();
    public static ITriggerEntry activeFunction = null;

    public static void run(
            String triggerObject,
            Boolean isBefore,
            Boolean isAfter,
            Boolean isInsert,
            Boolean isUpdate,
            Boolean isDelete,
            Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        // Ejecución de Funciones Transversales como logging, control de excepciones etc.
        transversalFunctionsAtBeginning();

        //Detección de la re-entrada en la clase en curso
        if (activeFunction != null) {
            activeFunction.inProgressEntry(triggerObject,
                    isBefore, isAfter, isInsert, isUpdate, isDelete, isExecuting,
                    newList, newMap, oldList, oldMap);

            System.debug('-ege- Re-entrada para objeto ' + triggerObject);

            return;
        }

        ////Routing para el objeto ACCOUNT a su Handler

        if (triggerObject == 'Account') {
            activeFunction = new AccountEntryHandler();

            activeFunction.mainEntry(triggerObject,
                    isBefore, isAfter, isInsert, isUpdate, isDelete, isExecuting,
                    newList, newMap, oldList, oldMap);

            if (acsToUpdate.size() > 0) update acsToUpdate.values();

            activeFunction = null;
        }

        //Routing para el objeto OPPORTUNITY a su Handler

        if (triggerObject == 'Opportunity') {
            activeFunction = new OpportunityEntryHandler();

            //Invoación a la función del Handler de primera entrada
            activeFunction.mainEntry(triggerObject,
                    isBefore, isDelete, isAfter, isInsert,
                    isUpdate, isExecuting,
                    newList, newMap, oldList, oldMap);

            activeFunction = null;
        }

        // Ejecución de Funciones Transversales como logging, control de excepciones etc.
        transversalFunctionsAtEnding();
    }

    private static void transversalFunctionsAtBeginning(){}

    private static void transversalFunctionsAtEnding(){}


}