/*
 * @author Esteve Graells
 * @date: junio 2018
 * @description: Implantación de la lógica de negocio para la gestión de los
 * triggers de Opportuntiy
 *
 */

public class OpportunitytEntryHandler implements iTriggerEntry {

    public void mainEntry(String triggerObject, Boolean isBefore,
            Boolean isDelete, Boolean isAfter, Boolean isInsert,
            Boolean isUpdate, Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        List<Opportunity> opNewList = (List<Opportunity>) newList;
        List<Opportunity> opOldList = (List<Opportunity>) oldList;
        Map<ID, Opportunity> opNewMap = (Map<ID, Opportunity>) newMap;
        Map<ID, Opportunity> opOldMap = (Map<ID, Opportunity>) oldMap;

        if (triggerObject == 'Opportunity'){

            //Logica de negocio, tenemos las variables de naturaleza del
            //evento: Is*, y tenemos los Maps y las Lists correspondientes
            //por lo que podemos construir

        }
    }

    //Método que gestiona la re-enetrada
    public void inProgressEntry(String triggerObject, Boolean isBefore,
            Boolean isDelete, Boolean isAfter, Boolean isInsert,
            Boolean isUpdate, Boolean isExecuting,
            List<SObject> newList, Map<ID, SObject> newMap,
            List<SObject> oldList, Map<ID, SObject> oldMap) {

        if (TriggerObject == 'Opportunity') {

            //Logica de negocio

        }
    }
}