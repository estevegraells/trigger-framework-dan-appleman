/*
* @author: Esteve Graells
* @date: Junio 2018
* @description: Clase de Test para Trigger Account con Framework Dan Appleman
*
*/

@isTest
private class AccountTriggerTestAppleman {

    static integer accountsToCreate = 300;
    static List<Account> accounts = new List<Account>();
    static List<Opportunity> opps = new List<Opportunity>();



    @isTest
    private static void testTrigger() {

        Test.startTest();

        //////////////////////////////////////////////
        //Test para el evento de INSERT sobre N Accounts
        //////////////////////////////////////////////
        testInsertAccounts();

        //////////////////////////////////////////////
        //Test para el evento de INSERT sobre Opportunities
        //////////////////////////////////////////////
        testInsertOpportunities();

        //////////////////////////////////////////////
        //Test para el evento de UPDATE sobre Accounts
        /////////////////////////////////////////////
        testUpdateAccounts();

        ////////////////////////////////
        //TEST del evento de DELETE sobre Accounts (Ninguna de las accounts ha sido borrada, pq no superan la validación en el Handler)
        ////////////////////////////////
        testDeleteAccountsWithOpps();

        Test.stopTest();
    }

    private static void testDeleteAccountsWithOpps() {
        List<Database.DeleteResult> resultsDeleteAccounts = Database.delete(accounts, false);
        for (Database.DeleteResult r : resultsDeleteAccounts) {
            if (r.isSuccess()) System.assert(r.getErrors().size() == 0); else System.debug('-ege- Errors ' + r.getErrors());
        }


        List<Account> currentAccounts = [SELECT id FROM Account];
        System.assert(currentAccounts.size() == accountsToCreate);
    }

    private static void testUpdateAccounts() {
        for (Account a : accounts) {
            a.Name = 'TestingUpdate';
        }

        List<Database.SaveResult> resultsUpdateAccounts = Database.update(accounts);

        List<AggregateResult> accountsUpdated = [SELECT count(id) totalUpdated FROM Account WHERE Name LIKE '%TestingUpdate'];
        System.assert(accountsUpdated[0].get('totalUpdated') == accounts.size());
    }

    private static void testInsertOpportunities() {
        for (Account a : accounts) {
            Opportunity o = new Opportunity(
                    Name = 'Testing',
                    StageName = 'Prospecting',
                    CloseDate = Date.today(),
                    AccountId = a.Id);

            opps.add(o);
        }

        List<Database.SaveResult> resultInsertOpportunities = Database.insert(opps, false);
        for (Database.SaveResult r : resultInsertOpportunities) {
            if (r.isSuccess()) System.assert(r.getErrors().size() == 0); else System.debug('-ege- Errors ' + r.getErrors());
        }

        System.assert(resultInsertOpportunities.size() == accountsToCreate);
    }

    private static void testInsertAccounts() {
        for (integer i = 0; i < accountsToCreate; i++) {
            Account acc = new Account(Name = 'Testing' + i);
            accounts.add(acc);
        }

        List<Database.SaveResult> resultsInsertAccounts = Database.insert(accounts, false);

        for (Database.SaveResult r : resultsInsertAccounts) {
            if (r.isSuccess()) System.assert(r.getErrors().size() == 0); else System.debug('-ege- Errors ' + r.getErrors());
        }

        System.assert(resultsInsertAccounts.size() == accountsToCreate);
    }
}